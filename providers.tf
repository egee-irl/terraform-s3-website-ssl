terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 5.32"
    }
  }
}

# Amazon Certificate Manager generates certificates in us-east-1 by default.
provider "aws" {
  alias  = "acm_east_1"
  region = "us-east-1"
}

provider "aws" {
  region = "us-west-2"
}
