#########################################################
## Reference architecture for an S3 bucket website    ##
## using CloudFront as a cutting edge (pun-intended) ##
## with a global edge network cache and SSL!       ##
#####################################################
locals {
  supported_protocols  = ["TLSv1", "TLSv1.1", "TLSv1.2"]
  min_protocol_version = "TLSv1.2_2019"
  default_web_root     = "index.html"
  error_document       = "404.html"
  cookies_forward      = "none"
  ipv6_support         = true
  s3_origin_id         = "demoOrigin"
  domain               = "egee.io"
}

## Domain is a data source (not created here) and must be supplied as a local. Could also be extracted to a tfvar.
data "aws_route53_zone" "demo" { name = local.domain }

## Certificate comes from AWS Certificate Manager and is created elsewhere and referenced here as a data source.
## Note: Certificates are generated in use1 by default! Double check your region.
data "aws_acm_certificate" "demo" {
  domain      = local.domain
  most_recent = true
  provider    = aws.acm_east_1
}

######################################################
## AWS S3 Bucket & Metadata for the static website. ##
######################################################
resource "aws_s3_bucket" "demo" {
  bucket = local.domain
}

resource "aws_s3_bucket_website_configuration" "s3_website" {
  bucket = aws_s3_bucket.demo.id

  index_document {
    suffix = local.default_web_root
  }

  error_document {
    key = local.error_document
  }
}

resource "aws_s3_bucket_acl" "demo" {
  bucket = aws_s3_bucket.demo.id
  acl    = "private"
}

#######################################################
## Cloudfront Distribution & Route53 A record to     ##
## allow s3 to behave like a proper web server host. ##
#######################################################
resource "aws_cloudfront_distribution" "demo" {
  origin {
    domain_name = aws_s3_bucket_website_configuration.s3_website.website_endpoint
    origin_id   = local.s3_origin_id

    custom_origin_config {
      http_port = 80
      https_port = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = local.supported_protocols
    }
  }

  enabled             = true
  is_ipv6_enabled     = local.ipv6_support
  default_root_object = local.default_web_root


  aliases = [local.domain]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = false
      cookies {
        forward = local.cookies_forward
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = data.aws_acm_certificate.demo.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = local.min_protocol_version
  }
}

#######################################################
## Route53 a record for redirect to the S3 bucekt   ##
## allow s3 to behave like a proper reverse proxy. ##
####################################################
resource "aws_route53_record" "demo_redir" {
  zone_id = data.aws_route53_zone.demo.id
  name    = local.domain
  type    = "A"

  alias {
    name    = aws_cloudfront_distribution.demo.domain_name
    zone_id = aws_cloudfront_distribution.demo.hosted_zone_id
    evaluate_target_health = false
  }
}

##############################################
## Assorted domain records for web mastery. ##
##############################################
resource "aws_route53_record" "demo_txt" {
  zone_id = data.aws_route53_zone.demo.id
  name    = "@"
  records = ["Hello World"]
  type    = "TXT"
  ttl     = "600"
}
