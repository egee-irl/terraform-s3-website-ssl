# terraform-s3-website-ssl

This is a reference architecture written with Terraform for using an S3 bucket to serve a website with CloudFront as a SSL-enabled reverse proxy. Specifically, this project:

- Creates an new AWS S3 bucket with the proper configuration to be used for a website
- Creates a CloudFront Distribution using an existing ssl cert from AWS Certificate Manager
- Creates a Route53 A record to redirect through CloudFront

It's currently written as a stand-alone Terraform project with locals for vars. It could be easily fashioned into a standard Terraform module.

It requires two inputs to work: A Route53 domain and an existing Amazon SSL Certificate

A working example of this project with CI/CD can be found [here](https://gitlab.com/egee-irl/egee-xyz).
